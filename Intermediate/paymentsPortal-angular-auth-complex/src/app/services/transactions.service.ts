import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Transaction } from '../interfaces/transaction';

@Injectable({
  providedIn: 'root',
})
export class TransactionsService {
  url: string = 'http://localhost:3000/transactions';

  constructor(private http: HttpClient) {}

  getTransactions(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(this.url);
  }

  getOneTransaction(id: string): Observable<Transaction> {
    return this.http.get<Transaction>(this.url + '/' + id);
  }

  updateTrasaction(data: Transaction, id: string): Observable<any> {
    return this.http.patch(`${this.url}/${id}`, data);
  }
}
