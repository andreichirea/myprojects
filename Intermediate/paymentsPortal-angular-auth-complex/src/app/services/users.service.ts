import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interfaces/user';
@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}
  url: string = 'http://localhost:3000/users';

  httpHeaders = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  opened: boolean;
  openedNavi: boolean = true;

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.url);
  }
  getOneUser(id: number): Observable<User> {
    return this.http.get<User>(this.url + '/' + id);
  }
  updateUser(data: User, id: number): Observable<any> {
    return this.http.patch(`${this.url}/${id}`, data);
  }
  /// Auth users

  getAuthUsers() {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      // prettier-ignore
      'Authorization': 'Bearer ' + sessionStorage.getItem('token'),
    });
    return this.http.get('http://localhost:3000/users', { headers: reqHeader });
    // return this.http.get('http://localhost:3000/users', );
  }
  getAuthUserByEmail(email, body) {
    return this.http.post('http://localhost:3000/login/' + email, body);
  }
  registerUser(data) {
    return this.http.post('http://localhost:3000/register', data);
  }

  isLoggedIn() {
    return sessionStorage.getItem('token') != null;
  }
}
