import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  url: string = 'http://localhost:3000/products';

  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url);
  }

  getOneProduct(id: number): Observable<Product> {
    return this.http.get<Product>(this.url + '/' + id);
  }

  updateProduct(data: Product, id: number): Observable<any> {
    return this.http.patch(`${this.url}/${id}`, data);
  }
}
