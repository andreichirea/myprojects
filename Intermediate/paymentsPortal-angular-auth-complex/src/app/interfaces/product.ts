import { FormGroup, FormControl } from '@angular/forms';

export interface Product {
  id: string;
  quantity: number;
  name: string;
  price: number;
  vatProd: number;
  totalPrice: number;
}
