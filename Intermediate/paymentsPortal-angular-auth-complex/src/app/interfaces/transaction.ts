import { Product } from './product';

export interface Transaction {
  id: any;

  products: Product[];
  details: {
    createdBy: string;
    createdAt: string;
    status: any;
    price: any;
    email: string;
    vat: number;
    totalPrice: number;
  };
}
