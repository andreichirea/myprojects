export interface User {
  id: number;
  name: string;
  email: string;
  role: string;
  tasks: number;
  task_per_month: [number];
  phone: string;
  schedule_today: [{ name: string; date: string; location?: string }];
  schedule_tomorrow: [{ name: string; date: string; location?: string }];
}
