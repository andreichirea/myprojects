import { Component, DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private usersService: UsersService, private router: Router) {}

  get openedNavi(): boolean {
    return this.usersService.openedNavi;
  }
  set openedNavi(value: boolean) {
    this.usersService.openedNavi = value;
  }
}
