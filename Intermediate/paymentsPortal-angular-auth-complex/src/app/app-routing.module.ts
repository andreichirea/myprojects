import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GuardGuard } from './components/auth/guard.guard';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { WelcomeComponent } from './components/welcome/welcome.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: 'welcome',
        component: WelcomeComponent,
        canActivate: [GuardGuard],
      },
      { path: 'register', component: RegisterComponent },
      { path: 'login', component: LoginComponent },
      {
        path: 'users',
        component: UsersListComponent,
        canActivate: [GuardGuard],
        children: [
          {
            path: ':id',
            component: UsersDetailComponent,
          },
          { path: ':id/edit', component: UserEditComponent },
        ],
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [GuardGuard],
      },
      {
        path: 'transactions',
        component: TransactionsComponent,
        canActivate: [GuardGuard],
      },

      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full',
      },
      {
        path: '**',
        component: PagenotfoundComponent,
        canActivate: [GuardGuard],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
