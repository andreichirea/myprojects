import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { HeaderComponent } from './components/header/header.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatInput, MatInputModule } from '@angular/material/input';
import {
  MyErrorStateMatcher,
  UserEditComponent,
} from './components/user-edit/user-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { NavigationComponent } from './components/navigation/navigation.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { usersReducer } from './components/state/users/users.reducer';
import { UsersModule } from './components/users-list/users/users.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { TransactionsViewComponent } from './components/transactions/transactions-view/transactions-view.component';
import { TransactionsEditComponent } from './components/transactions/transactions-edit/transactions-edit.component';

import { ProductsShellComponent } from './components/products-shell/products-shell.component';
import { ProductsModule } from './components/products-shell/products.module';
import { ProductsViewComponent } from './components/products-shell/products-view/products-view.component';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { TransactionsModule } from './components/transactions/transactions.module';
import { FormArrayComponent } from './shared/form-array/form-array.component';
import { FormInputComponent } from './shared/form-input/form-input.component';
import { InputNumberComponent } from './shared/input-number/input-number.component';
import { InputDateComponent } from './shared/input-date/input-date.component';
import { InputEmailComponent } from './shared/input-email/input-email.component';
import { NgChartsModule } from 'ng2-charts';
import { ChartdoughnutComponent } from './components/chartdoughnut/chartdoughnut.component';
import { ChartlineComponent } from './components/chartline/chartline.component';
import { ChartpieComponent } from './components/chartpie/chartpie.component';
import { SmallBoxComponent } from './components/small-box/small-box.component';
import { FooterComponent } from './components/footer/footer.component';
import { TransPriceComponent } from './components/transactions/trans-price/trans-price.component';
import { TransactionDetailsComponent } from './components/transactions/transaction-details/transaction-details.component';
import { MatMenuModule } from '@angular/material/menu';
import { ToastrModule } from 'ngx-toastr';
import { RegisterComponent } from './components/auth/register/register.component';
import { LoginComponent } from './components/auth/login/login.component';
import { PopupComponent } from './components/auth/popup/popup.component';
import { ModalConfirmComponent } from './components/overlay/modal-confirm/modal-confirm.component';
import { PortalModule } from '@angular/cdk/portal';
import { DrawerComponent } from './components/drawer/drawer/drawer.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    UsersDetailComponent,
    HeaderComponent,
    TransactionsComponent,
    SettingsComponent,
    PagenotfoundComponent,
    WelcomeComponent,
    UserEditComponent,
    NavigationComponent,
    TransactionsViewComponent,
    TransactionsEditComponent,
    ProductsShellComponent,
    ProductsViewComponent,
    FormArrayComponent,
    FormInputComponent,
    InputNumberComponent,
    InputDateComponent,
    InputEmailComponent,
    ChartdoughnutComponent,
    ChartlineComponent,
    ChartpieComponent,
    SmallBoxComponent,
    FooterComponent,
    TransPriceComponent,
    TransactionDetailsComponent,
    RegisterComponent,
    LoginComponent,
    PopupComponent,
    ModalConfirmComponent,
    DrawerComponent,
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatIconModule,
    MatSelectModule,
    ProductsModule,
    MatToolbarModule,
    CdkAccordionModule,
    BrowserAnimationsModule,
    FormsModule,
    MatInputModule,
    // MyErrorStateMatcher,
    MatTableModule,
    MatFormFieldModule,
    MatSortModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    AppRoutingModule,
    MatMenuModule,
    PortalModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
    }),
    UsersModule,
    TransactionsModule,

    TranslateModule.forRoot({
      defaultLanguage: 'en-US',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    StoreModule.forRoot({}, {}),
    StoreDevtoolsModule.instrument({
      name: 'paymentsPortal app Devtools',
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
    NgChartsModule,
  ],
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
