import {
  ProductsState,
  TransactionsState,
  UsersState,
} from '../components/state/users/users.interfaces';

export interface State {
  users: UsersState;
  transactions: TransactionsState;
  products: ProductsState;
}
