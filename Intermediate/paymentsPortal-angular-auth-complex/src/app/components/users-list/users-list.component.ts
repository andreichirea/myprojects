import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter, Observable, Subscription } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';
import { State } from 'src/app/appState/app.state';
import {
  getCurrentUser,
  getShowInfo,
  getUsers,
} from '../state/users/users.selectors';
import * as UserActions from '../state/users/users.actions';
import { User } from 'src/app/interfaces/user';
import { DrawerService } from 'src/app/services/drawer.service';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit, OnDestroy {
  dataSource$: Observable<User[]>;
  obs: Subscription;
  id: number;
  displayCode$: Observable<boolean>;
  reduxCheckObs: Subscription;
  currentUser$: Observable<User>;

  get opened(): boolean {
    return this.usersService.opened;
  }
  set opened(value: boolean) {
    this.usersService.opened = value;
  }

  displayedColumns: string[] = ['id', 'name', 'email', 'role'];

  constructor(
    private usersService: UsersService,
    private router: Router,
    private store: Store<State>,
    private drawerService: DrawerService
  ) {
    this.obs = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        if (this.router.url === '/users') {
          this.store.dispatch(UserActions.getUsers());
          this.opened = false;
        }
      });
  }
  checkChanged() {
    this.store.dispatch(UserActions.toggleInfo());
  }

  editUser(user: User) {
    this.opened = true;
    this.store.dispatch(UserActions.setCurrentUser({ currentUserId: user.id }));
    this.openDrawer();
  }

  ngOnInit(): void {
    this.dataSource$ = this.store.select(getUsers);
    this.store.dispatch(UserActions.getUsers());

    this.displayCode$ = this.store.select(getShowInfo);
    this.currentUser$ = this.store.select(getCurrentUser);
  }
  ngOnDestroy(): void {
    this.obs.unsubscribe();
  }
  openDrawer() {
    this.drawerService.open();
  }
}
