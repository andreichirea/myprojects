import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TransactionsState } from '../users/users.interfaces';

const getTransactionsFeatureState =
  createFeatureSelector<TransactionsState>('transactions');

export const getTransactions = createSelector(
  getTransactionsFeatureState,
  (state) => state.transactions
);
export const getTransactionError = createSelector(
  getTransactionsFeatureState,
  (state) => state.error
);
export const getCurrentTransactionId = createSelector(
  getTransactionsFeatureState,
  (state) => state.currentTransactionId
);

export const getCurrentTransaction = createSelector(
  getTransactionsFeatureState,
  getCurrentTransactionId,
  (state, currentId) =>
    currentId ? state.transactions.find((p) => p.id === currentId) : null
);
export const getCurrentTransactionProducts = createSelector(
  getTransactionsFeatureState,
  (state) => state.transactions.map((p) => p)
);
