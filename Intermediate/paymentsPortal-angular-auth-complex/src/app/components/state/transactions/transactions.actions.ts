import { createAction, props } from '@ngrx/store';
import { Transaction } from 'src/app/interfaces/transaction';

export const getTransactions = createAction('[Users] load all transactions');

export const getTransactionsSucces = createAction(
  '[Transactions] get all transactions Succes',
  props<{ transactions: Transaction[] }>()
);
export const getTransactionsFail = createAction(
  '[Transactions] get all transactions Fail',
  props<{ err: string }>()
);
export const setCurrentTransaction = createAction(
  '[Transactions] Set current transaction',
  props<{ currentTransactionId: string }>()
);

export const updateTransaction = createAction(
  '[Transactions] Update transaction',
  props<{ transaction: Transaction }>()
);
export const updateTransactionSucces = createAction(
  '[Transactions] Update transaction Succes',
  props<{ transaction: Transaction }>()
);
export const updateTransactionFailed = createAction(
  '[Transactions] Update transaction failed',
  props<{ error: string }>()
);
