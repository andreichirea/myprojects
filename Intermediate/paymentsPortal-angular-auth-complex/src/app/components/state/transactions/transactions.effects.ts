import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, map, mergeMap, of, tap } from 'rxjs';
import { TransactionsService } from 'src/app/services/transactions.service';
import * as TransactionsAction from '../transactions/transactions.actions';
@Injectable()
export class TransactionsEffects {
  constructor(
    private actions$: Actions,
    private transactionsService: TransactionsService
  ) {}
  loadTransactions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TransactionsAction.getTransactions),
      mergeMap(() =>
        this.transactionsService.getTransactions().pipe(
          map((transactions) =>
            TransactionsAction.getTransactionsSucces({ transactions })
          ),
          catchError((err) =>
            of(TransactionsAction.getTransactionsFail({ err }))
          )
        )
      )
    );
  });
  updateTransaction$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TransactionsAction.updateTransaction),
      concatMap((action) =>
        this.transactionsService
          .updateTrasaction(action.transaction, action.transaction.id)
          .pipe(
            map((trans) =>
              TransactionsAction.updateTransactionSucces({ transaction: trans })
            ),

            catchError((err) =>
              of(TransactionsAction.updateTransactionFailed({ error: err }))
            )
          )
      )
    );
  });
}
