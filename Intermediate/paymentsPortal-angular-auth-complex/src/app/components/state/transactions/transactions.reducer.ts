import { createReducer, on } from '@ngrx/store';
import { TransactionsState } from '../users/users.interfaces';
import * as TransactionsAction from './transactions.actions';
const initialTransactionState: TransactionsState = {
  showInfo: false,
  currentTransactionId: null,
  transactions: [],
  error: '',
};
export const TransactionsReducer = createReducer<TransactionsState>(
  initialTransactionState,
  on(
    TransactionsAction.getTransactionsSucces,
    (state, action): TransactionsState => {
      return {
        ...state,
        transactions: action.transactions,
        error: '',
      };
    }
  ),
  on(
    TransactionsAction.getTransactionsFail,
    (state, action): TransactionsState => {
      return {
        ...state,
        transactions: [],
        error: action.err,
      };
    }
  ),
  on(
    TransactionsAction.setCurrentTransaction,
    (state, action): TransactionsState => {
      return {
        ...state,
        currentTransactionId: action.currentTransactionId,
      };
    }
  ),
  on(
    TransactionsAction.updateTransactionSucces,
    (state, action): TransactionsState => {
      const updatedTrans = state.transactions.map((item) =>
        action.transaction.id === item.id ? action.transaction : item
      );
      return {
        ...state,
        transactions: updatedTrans,
        currentTransactionId: action.transaction.id,
        error: '',
      };
    }
  ),
  on(
    TransactionsAction.updateTransactionFailed,
    (state, action): TransactionsState => {
      return {
        ...state,
        error: action.error,
      };
    }
  )
);
