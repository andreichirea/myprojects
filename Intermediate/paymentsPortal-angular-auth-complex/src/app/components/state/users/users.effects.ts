import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';

import { catchError, concatMap, map, mergeMap, of, tap } from 'rxjs';

import { UsersService } from 'src/app/services/users.service';
import * as UserAction from '../users/users.actions';
@Injectable()
export class UsersEffects {
  constructor(private actions$: Actions, private usersService: UsersService) {}

  loadUsers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserAction.getUsers),
      mergeMap(() =>
        this.usersService.getUsers().pipe(
          map((users) => UserAction.getUsersSucces({ users })),
          catchError((err) => of(UserAction.getUsersFail({ err })))
        )
      )
    );
  });

  updateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserAction.updateUser),
      concatMap((action) =>
        this.usersService.updateUser(action.user, action.user.id).pipe(
          map((user) => UserAction.updateUserSucces({ user })),
          catchError((error) => of(UserAction.updateUserFail({ error })))
        )
      )
    );
  });
}
