import { Product } from 'src/app/interfaces/product';
import { Transaction } from 'src/app/interfaces/transaction';
import { User } from 'src/app/interfaces/user';

export interface UsersState {
  showInfo: boolean;
  currentUserId: number | null;
  users: User[];
  error: string;
}

export interface TransactionsState {
  showInfo: boolean;
  currentTransactionId: string | null;

  transactions: Transaction[];
  error: string;
}

export interface ProductsState {
  products: Product[];
  currentProduct: Product;
  error: string;
}
