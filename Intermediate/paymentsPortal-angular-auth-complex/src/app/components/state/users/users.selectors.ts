import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UsersState } from './users.interfaces';

const getUserFeatureState = createFeatureSelector<UsersState>('users');

export const getShowInfo = createSelector(
  getUserFeatureState,
  (state) => state.showInfo
);

export const getCurrentUserId = createSelector(
  getUserFeatureState,
  (state) => state.currentUserId
);
export const getCurrentUser = createSelector(
  getUserFeatureState,
  getCurrentUserId,
  (state, currentUserId) =>
    currentUserId ? state.users.find((user) => user.id === currentUserId) : null
);

export const getUsers = createSelector(
  getUserFeatureState,
  (state) => state.users
);
export const getError = createSelector(
  getUserFeatureState,
  (state) => state.error
);
