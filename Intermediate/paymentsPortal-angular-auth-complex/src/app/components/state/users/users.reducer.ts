import {
  createAction,
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import { User } from 'src/app/interfaces/user';
import * as UserAction from './users.actions';
import { UsersState } from './users.interfaces';

const initialState: UsersState = {
  showInfo: true,
  currentUserId: null,
  users: [],
  error: '',
};

export const usersReducer = createReducer<UsersState>(
  initialState,
  on(UserAction.toggleInfo, (state): UsersState => {
    return {
      ...state,
      showInfo: !state.showInfo,
    };
  }),
  on(UserAction.setCurrentUser, (state, action): UsersState => {
    return {
      ...state,
      currentUserId: action.currentUserId,
    };
  }),
  on(UserAction.clearCurrentUser, (state): UsersState => {
    return {
      ...state,
      currentUserId: null,
    };
  }),
  on(UserAction.getUsersSucces, (state, action): UsersState => {
    return {
      ...state,
      users: action.users,
      error: '',
    };
  }),
  on(UserAction.getUsersFail, (state, action): UsersState => {
    return {
      ...state,
      users: [],
      error: action.err,
    };
  }),
  //update users
  on(UserAction.updateUserSucces, (state, action): UsersState => {
    const updatedUsers = state.users.map((item) =>
      action.user.id === item.id ? action.user : item
    );
    return {
      ...state,
      users: updatedUsers,
      currentUserId: action.user.id,
      error: '',
    };
  }),
  on(UserAction.updateUserFail, (state, action): UsersState => {
    return {
      ...state,
      error: action.error,
    };
  })
);
