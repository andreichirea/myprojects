import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/interfaces/user';

export const toggleInfo = createAction('[Users] Toggle users Info');
export const setCurrentUser = createAction(
  '[Users] Set current user',
  props<{ currentUserId: number }>()
);
export const clearCurrentUser = createAction('[Users] Clear current user');
//load users data
export const getUsers = createAction('[Users] load all users');
export const getUsersSucces = createAction(
  '[Users] get all users Succes',
  props<{ users: User[] }>()
);
export const getUsersFail = createAction(
  '[Users] get all users Fail',
  props<{ err: string }>()
);
//update users
export const updateUser = createAction(
  '[Users] update user',
  props<{ user: User }>()
);
export const updateUserSucces = createAction(
  '[Users] update user succes',
  props<{ user: User }>()
);
export const updateUserFail = createAction(
  '[Users] update user Failed',
  props<{ error: string }>()
);
