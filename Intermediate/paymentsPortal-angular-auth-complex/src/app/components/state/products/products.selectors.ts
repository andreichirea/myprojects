import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProductsState } from '../users/users.interfaces';

const getProductsFeatureState =
  createFeatureSelector<ProductsState>('products');

export const getProducts = createSelector(
  getProductsFeatureState,
  (state) => state.products
);
export const getProductsError = createSelector(
  getProductsFeatureState,
  (state) => state.error
);
