import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { ProductsService } from 'src/app/services/products.service';
import * as ProductsAction from '../products/products.action';
@Injectable()
export class ProductsEffects {
  constructor(
    private actions$: Actions,
    private productsService: ProductsService
  ) {}
  loadproducts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProductsAction.getProducts),
      mergeMap(() =>
        this.productsService.getProducts().pipe(
          map((products) => ProductsAction.getProductsSucces({ products })),

          catchError((err) =>
            of(
              ProductsAction.getProductsFail({ error: 'Something went wrong' })
            )
          )
        )
      )
    );
  });
}
