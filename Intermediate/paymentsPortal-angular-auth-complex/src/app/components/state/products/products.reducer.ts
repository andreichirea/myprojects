import { createReducer, on } from '@ngrx/store';
import { ProductsState } from '../users/users.interfaces';
import * as ProductsAction from './products.action';
const initialProductsState: ProductsState = {
  products: [],
  currentProduct: null,
  error: '',
};
export const ProductsReducer = createReducer<ProductsState>(
  initialProductsState,
  on(ProductsAction.getProductsSucces, (state, action): ProductsState => {
    return {
      ...state,
      products: action.products,
    };
  }),
  on(ProductsAction.getProductsFail, (state, action): ProductsState => {
    return {
      ...state,
      error: action.error,
    };
  })
);
