import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/interfaces/product';

export const getProducts = createAction('[Products] load all products');

export const getProductsSucces = createAction(
  '[products] get all products Succes',
  props<{ products: Product[] }>()
);
export const getProductsFail = createAction(
  '[products] get all products Fail',
  props<{ error: string }>()
);
