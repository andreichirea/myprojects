import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements DoCheck {
  constructor(
    private userService: UsersService,
    private translateService: TranslateService,
    private router: Router
  ) {}
  isMenuRequired = false;

  ngDoCheck(): void {
    let currentUrl = this.router.url;
    if (currentUrl === '/login' || currentUrl === '/register') {
      this.isMenuRequired = false;
    } else {
      this.isMenuRequired = true;
    }
  }

  openMenu() {
    this.userService.openedNavi = !this.userService.openedNavi;
  }
  selectLanguage(event) {
    this.translateService.use(event.value);
  }
}
