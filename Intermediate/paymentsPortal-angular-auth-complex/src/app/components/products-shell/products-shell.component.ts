import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from 'src/app/appState/app.state';
import { Product } from 'src/app/interfaces/product';
import { Transaction } from 'src/app/interfaces/transaction';
import * as ProductsActions from '../state/products/products.action';
import { getProducts } from '../state/products/products.selectors';
import { getCurrentTransaction } from '../state/transactions/transactions.selectors';
import * as TransactionActions from '../state/transactions/transactions.actions';
@Component({
  selector: 'app-products-shell',
  templateUrl: './products-shell.component.html',
  styleUrls: ['./products-shell.component.scss'],
})
export class ProductsShellComponent implements OnInit {
  currentProducts;
  constructor(private store: Store<State>) {}
  productsForm: FormGroup;
  ngOnInit(): void {
    this.productsForm = new FormGroup({
      vatProd: new FormControl('', [Validators.required]),

      price: new FormControl('', [Validators.required]),

      quantity: new FormControl('', [Validators.required]),
    });
  }
  editProd(data) {
    this.productsForm.patchValue({
      ...data,
      vat: data.vatProd,
      quantity: data.quantity,
      price: data.price,
    });
  }
  getProducts() {
    this.store.select(getCurrentTransaction).subscribe(
      (data) => {
        this.currentProducts = data.products;
        console.log('from child prodddd', this.currentProducts);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  saveProduct(trans) {
    console.log(trans);
    if (this.productsForm.valid) {
      const newTRans: Transaction = {
        ...trans,
        products: this.productsForm,
      };
      this.store.dispatch(
        TransactionActions.updateTransaction({ transaction: newTRans })
      );
      console.log(newTRans);
    }
  }
  getOneProduct(item) {
    this.editProd(item);
  }
}
