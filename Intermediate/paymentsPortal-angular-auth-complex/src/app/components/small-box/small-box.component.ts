import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/appState/app.state';
import { getCurrentUser } from '../state/users/users.selectors';
declare var anime: any;
@Component({
  selector: 'app-smallbox',
  templateUrl: './small-box.component.html',
  styleUrls: ['./small-box.component.scss'],
})
export class SmallBoxComponent {
  user$;
  @Input() data;
  @Input() title;

  constructor(private store: Store<State>) {}
  ngOnInit(): void {
    this.getUser();
  }
  ngAfterViewInit(): void {
    let textWrapper = document.querySelector('.c1');
    textWrapper.innerHTML = textWrapper.textContent.replace(
      /\S/g,
      "<span class='el' style='display:inline-block;'>$&</span>"
    );

    anime
      .timeline({ loop: true })
      .add({
        targets: '.c1 .el',
        translateY: ['1.1em', 0],
        translateZ: 0,
        duration: 750,
        delay: (el, i) => 100 * i,
      })
      .add({
        targets: '.c1',
        opacity: 0,
        duration: 1000,
        easing: 'easeOutExpo',
        delay: 100,
      });
  }
  getUser() {
    this.store.select(getCurrentUser).subscribe(
      (data) => {
        this.user$ = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
