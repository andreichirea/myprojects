import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-chartdoughnut',
  templateUrl: './chartdoughnut.component.html',
  styleUrls: ['./chartdoughnut.component.scss'],
})
export class ChartdoughnutComponent {
  public SystemName: string = 'Expenses per date';
  firstCopy = false;
  @Input() date;
  @Input() data;
  toggle;
  // data
  public lineChartData: Array<number> = [3859, 8975, 10518, 1191, 252];

  public labelMFL: Array<any> = [
    { data: this.lineChartData, label: this.SystemName },
  ];
  // labels
  public lineChartLabels: Array<any> = [
    '	2018-03-22',
    '2018-03-14',
    '2018-03-22',
    '2019-03-22',
    '2020-03-22',
  ];

  constructor() {}
  toggleChart() {
    this.toggle = !this.toggle;
  }
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            max: 60,
            min: 0,
          },
        },
      ],
      xAxes: [{}],
    },
    plugins: {
      datalabels: {
        display: true,
        align: 'top',
        anchor: 'end',
        //color: "#2756B3",
        color: '#222',

        font: {
          family: 'FontAwesome',
          size: 11,
        },
      },
      deferred: false,
    },
  };

  _lineChartColors: Array<any> = [
    {
      backgroundColor: 'red',
      borderColor: 'red',
      pointBackgroundColor: 'red',
      pointBorderColor: 'red',
      pointHoverBackgroundColor: 'red',
      pointHoverBorderColor: 'red',
    },
  ];

  public ChartType = 'bar';

  public chartClicked(e: any): void {
    console.log(e);
  }
  public chartHovered(e: any): void {
    console.log(e);
  }
}
