import { Component, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { ChartConfiguration, ChartOptions } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { State } from 'src/app/appState/app.state';
import { getCurrentUser } from '../state/users/users.selectors';

@Component({
  selector: 'app-chartline',
  templateUrl: './chartline.component.html',
  styleUrls: ['./chartline.component.scss'],
})
export class ChartlineComponent {
  public lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: ['January', 'March', 'May', 'July', 'September', 'November'],
    datasets: [
      {
        data: [2, 4, 6, 8, 10, 12],
        label: 'Series A',
        fill: true,
        tension: 0.5,
        borderColor: 'black',
        backgroundColor: 'rgba(15,23,42,0.4)',
      },
    ],
  };
  public lineChartOptions: ChartOptions<'line'> = {
    responsive: false,
  };
  public lineChartLegend = true;
  user$;
  @ViewChild(BaseChartDirective) private _chart;

  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.getUser();
  }
  getUser() {
    this.store.select(getCurrentUser).subscribe(
      (data) => {
        this.user$ = data;

        setInterval(() => {
          this.lineChartData.datasets[0].data = data.task_per_month;
        }, 10);
        this._chart.chart.update();
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
