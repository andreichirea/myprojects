import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/appState/app.state';
import { Transaction } from 'src/app/interfaces/transaction';
import { getCurrentTransaction } from '../../state/transactions/transactions.selectors';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.scss'],
})
export class TransactionDetailsComponent {
  currTransaction: Transaction;
  productsPrice = [];

  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.store.select(getCurrentTransaction).subscribe((data) => {
      this.currTransaction = data;
    });
  }
}
