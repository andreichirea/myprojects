import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { State } from 'src/app/appState/app.state';
import { Transaction } from 'src/app/interfaces/transaction';
import { getCurrentTransaction } from '../../state/transactions/transactions.selectors';
import * as TransactionActions from '../../state/transactions/transactions.actions';
import { Product } from 'src/app/interfaces/product';

@Component({
  selector: 'app-transactions-edit',
  templateUrl: './transactions-edit.component.html',
  styleUrls: ['./transactions-edit.component.scss'],
})
export class TransactionsEditComponent implements OnInit {
  transactionForm: FormGroup;
  currTransaction: Transaction;
  placeholderValues = ['Your name', 'VAT', 'Created at date', 'Your Email'];
  get productsFormArray() {
    return (<FormArray>this.transactionForm.get('products')) as FormArray;
  }

  get totalPrice() {
    return this.transactionForm.get('totalPrice');
  }
  get details() {
    return (<FormGroup>this.transactionForm.get('details')) as FormGroup;
  }

  constructor(private store: Store<State>, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.store.select(getCurrentTransaction).subscribe((data) => {
      this.currTransaction = data;
      this.createInitialForm(data);

      this.buildProductsGRP(data.products);
      this.modifyMyProds(data.products);
      this.calcTransPrice(data);
    });
  }

  createInitialForm(trans) {
    this.transactionForm = this.fb.group({
      details: this.fb.group({
        vat: [trans.details.vat, [Validators.required]],
        createdAt: [trans.details.createdAt, [Validators.required]],
        createdBy: [trans.details.createdBy, [Validators.required]],
        email: [trans.details.email, [Validators.required]],
      }),

      products: this.fb.array([]),
    });
  }

  buildProductsGRP(prods) {
    for (let i = 0; i < prods.length; i++) {
      this.productsFormArray.push(
        this.fb.group({
          quantity: [prods[i].quantity, Validators.required],
          price: [prods[i].price, Validators.required],
          vatProd: [prods[i].vatProd, Validators.required],
          id: [prods[i].id, Validators.required],
          name: [prods[i].name, Validators.required],
          totalPrice: [prods[i].totalPrice, Validators.required],
        })
      );
    }
  }

  modifyMyProds(data: Product[]) {
    for (let i = 0; i < data.length; i++) {
      this.productsFormArray.at(i).patchValue({
        id: data[i].id,
        quantity: data[i].quantity,
        name: data[i].name,
        price: data[i].price,
        vatProd: data[i].vatProd,
        totalPrice:
          data[i].price * data[i].quantity * parseFloat('1.' + data[i].vatProd),
      });
    }
  }

  sumPrices = 0;
  calcTransPrice(trans: Transaction) {
    let products = trans.products;
    this.sumPrices = products.reduce((p, c) => {
      return p + c.totalPrice;
    }, 0);

    return this.sumPrices;
  }
  save(trans: Transaction) {
    if (this.transactionForm.valid) {
      this.calcTransPrice(trans);
      if (trans.products) {
        this.modifyMyProds(trans.products);
        const newTRans: Transaction = {
          ...this.currTransaction,
          ...trans,
          details: {
            ...trans.details,
            price: this.sumPrices,
            status: 'COMPLETED',
          },
        };
        this.store.dispatch(
          TransactionActions.updateTransaction({ transaction: newTRans })
        );
      }
    }
  }
  onCancel() {
    this.transactionForm.reset();
  }
}
