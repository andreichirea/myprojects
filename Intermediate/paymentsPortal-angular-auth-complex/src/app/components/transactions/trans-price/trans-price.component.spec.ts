import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransPriceComponent } from './trans-price.component';

describe('TransPriceComponent', () => {
  let component: TransPriceComponent;
  let fixture: ComponentFixture<TransPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransPriceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
