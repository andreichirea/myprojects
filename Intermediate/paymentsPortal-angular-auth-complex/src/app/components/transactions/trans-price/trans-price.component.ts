import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-trans-price',
  templateUrl: './trans-price.component.html',
  styleUrls: ['./trans-price.component.scss'],
})
export class TransPriceComponent {
  @Input() name;
  @Input() data;
  @Input() title;
}
