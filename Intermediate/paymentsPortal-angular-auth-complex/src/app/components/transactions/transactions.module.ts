import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { TransactionsReducer } from '../state/transactions/transactions.reducer';
import { EffectsModule } from '@ngrx/effects';
import { TransactionsEffects } from '../state/transactions/transactions.effects';
import { TransactionsService } from '../../services/transactions.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    //TransactionsService,
    StoreModule.forFeature('transactions', TransactionsReducer),
    EffectsModule.forFeature([TransactionsEffects]),
  ],
})
export class TransactionsModule {}
