import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable, tap } from 'rxjs';
import { State } from 'src/app/appState/app.state';
import { Transaction } from 'src/app/interfaces/transaction';
import * as TransactionsActions from '../state/transactions/transactions.actions';
import { getTransactions } from '../state/transactions/transactions.selectors';
@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  openedTransaction = false;
  allTransactions$: Observable<Transaction[]>;
  displayedColumns = [
    'id',
    'createdBy',
    'email',
    'createdAt',
    'status',
    'price',

    'vat',
    'totalPrice',
    'products',
  ];
  current;
  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.store.dispatch(TransactionsActions.getTransactions());
    this.allTransactions$ = this.store.select(getTransactions).pipe(
      map((trans) =>
        trans.map((trans) => ({
          ...trans,
          details: {
            ...trans.details,
            totalPrice:
              trans.details.price * parseFloat('1.' + trans.details.vat),
          },
        }))
      )
    );
  }
}
