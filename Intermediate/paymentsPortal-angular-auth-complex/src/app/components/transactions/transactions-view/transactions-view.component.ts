import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from 'src/app/appState/app.state';
import { Transaction } from 'src/app/interfaces/transaction';
import * as TransactionActions from '../../state/transactions/transactions.actions';

@Component({
  selector: 'app-transactions-view',
  templateUrl: './transactions-view.component.html',
  styleUrls: ['./transactions-view.component.scss'],
})
export class TransactionsViewComponent {
  currentUser: Observable<Transaction>;
  current: Observable<Transaction>;

  @Input()
  dataSource$;

  @Input()
  displayedColumns;

  constructor(private store: Store<State>) {}

  editTransaction(row) {
    this.store.dispatch(
      TransactionActions.setCurrentTransaction({
        currentTransactionId: row.id,
      })
    );
  }
}
