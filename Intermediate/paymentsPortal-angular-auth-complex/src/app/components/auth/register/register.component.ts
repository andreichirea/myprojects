import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from 'src/app/services/users.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private userService: UsersService,
    private router: Router
  ) {}

  registerForm = this.fb.group({
    email: this.fb.control(
      '',
      Validators.compose([Validators.required, Validators.email])
    ),
    password: this.fb.control(
      '',
      Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(
          /^(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/
        ),
      ])
    ),
    passRepeat: this.fb.control(
      '',
      Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(
          /^(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/
        ),
        ,
      ])
    ),
    isActive: this.fb.control(true),
  });

  registerComplete() {
    console.log('registr', this.registerForm);
    if (this.registerForm.valid) {
      if (
        this.registerForm.controls.password.value ===
        this.registerForm.controls.passRepeat.value
      ) {
        this.userService.registerUser(this.registerForm.value).subscribe(
          (res) => {
            console.log('this is the response', res);
            this.toastr.success('Registration succes');
            this.router.navigate(['/login']);
            console.log(this.registerForm.value);
          },
          (err) => {
            console.log('error', err);
            this.toastr.warning(err.statusText);
          }
        );
      }
    } else {
      this.toastr.warning('Please enter a valid data');
    }
  }
}
