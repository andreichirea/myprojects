import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  userData;
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private userService: UsersService,
    private router: Router
  ) {
    sessionStorage.clear();
  }

  loginForm = this.fb.group({
    email: this.fb.control(
      '',
      Validators.compose([Validators.required, Validators.email])
    ),
    password: this.fb.control(
      '',
      Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(
          /^(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/
        ),
      ])
    ),
  });

  loginComplete() {
    if (this.loginForm.valid) {
      this.userService
        .getAuthUserByEmail(this.loginForm.value.email, this.loginForm.value)
        .subscribe(
          (res) => {
            this.userData = res;
            console.log('resData', res);
            console.log('useerData', this.userData);
            if (
              this.userData.user.passRepeat === this.loginForm.value.password
            ) {
              console.log('intra2');
              if (this.userData.user.isActive) {
                console.log('intra');
                sessionStorage.setItem('token', this.userData.accessToken);
                this.router.navigate(['/users']);
                console.log('intra3');
                this.userService.getAuthUsers();
              }
            } else {
              this.toastr.warning('Invalid credentials');
            }
          },
          (err) => {
            this.toastr.warning(err.statusText);
          }
        );
    } else {
      this.toastr.warning('Please enter a valid data');
    }
  }
}
