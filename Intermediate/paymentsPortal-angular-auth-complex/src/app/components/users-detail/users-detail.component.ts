import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { State } from 'src/app/appState/app.state';
import { User } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/services/users.service';
import { getCurrentUser } from '../state/users/users.selectors';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss'],
})
export class UsersDetailComponent implements OnInit, OnDestroy {
  user$;
  schedulesInfos;
  toggle = true;

  constructor(private store: Store<State>) {}

  paramObs: Subscription;

  ngOnInit(): void {
    this.getUser();
  }
  ngOnDestroy(): void {
    this.paramObs.unsubscribe();
  }

  getUser() {
    this.paramObs = this.store.select(getCurrentUser).subscribe(
      (data) => {
        this.user$ = data;
        this.schedulesInfos = data.schedule_today;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  scheduleToday() {
    this.schedulesInfos = this.user$.schedule_today;
    this.toggle = !this.toggle;
  }
  scheduleTomorrow() {
    this.schedulesInfos = this.user$.schedule_tomorrow;
    this.toggle = !this.toggle;
  }
}
