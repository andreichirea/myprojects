import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { CdkPortal, DomPortalOutlet } from '@angular/cdk/portal';
import {
  ApplicationRef,
  Component,
  ComponentFactoryResolver,
  Injector,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { DrawerService } from 'src/app/services/drawer.service';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss'],
})
export class DrawerComponent implements OnInit {
  @ViewChild('drawer', { static: true }) drawerTemplateRef: TemplateRef<any>;

  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    private drawerService: DrawerService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    private viewContainerRef: ViewContainerRef
  ) {}

  ngOnInit() {
    const portalOutlet = new DomPortalOutlet(
      document.body,
      this.componentFactoryResolver,
      this.appRef,
      this.injector
    );

    this.drawerService.isOpen$.subscribe((isOpen) => {
      if (isOpen) {
        this.openDrawer(portalOutlet);
      } else {
        this.closeDrawer();
      }
    });
  }

  openDrawer(portalOutlet: DomPortalOutlet) {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().left('0'),
      hasBackdrop: true,
      backdropClass: 'cdk-overlay-dark-backdrop',
    });

    const drawerPortal = new CdkPortal(
      this.drawerTemplateRef,
      this.viewContainerRef
    );
    this.overlayRef.attach(drawerPortal);
  }

  closeDrawer() {
    if (this.overlayRef) {
      this.overlayRef.detach();
    }
  }
}
