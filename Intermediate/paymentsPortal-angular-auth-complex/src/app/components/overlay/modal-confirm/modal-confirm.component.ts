import { Component, Inject } from '@angular/core';
import { DialogRef } from '../dialog-ref';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss'],
})
export class ModalConfirmComponent {
  constructor(private dialogRef: DialogRef) {}

  close() {
    this.dialogRef.close();
  }
  stay() {
    this.dialogRef.stay();
  }
}
