import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, Observable, Subscription } from 'rxjs';
import { User } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/services/users.service';
import { Store } from '@ngrx/store';
import { getCurrentUser } from '../state/users/users.selectors';
import * as UserAction from '../state/users/users.actions';
import { State } from 'src/app/appState/app.state';
import { DialogService } from 'src/app/services/overlay.service';
import { ModalConfirmComponent } from '../overlay/modal-confirm/modal-confirm.component';
import { DrawerService } from 'src/app/services/drawer.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit, OnDestroy {
  usersForm: FormGroup;
  user: User;
  id: number | null = null;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
  ]);
  roleFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
  ]);
  get opened(): boolean {
    return this.usersService.opened;
  }
  set opened(value: boolean) {
    this.usersService.opened = value;
  }

  allSubscriptions: Subscription[] = [];

  matcher = new MyErrorStateMatcher();

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<State>,
    private dialog: DialogService,
    private drawerService: DrawerService
  ) {
    this.allSubscriptions.push(
      this.route.paramMap.subscribe((params) => {
        this.id = +params.get('id');
        if (this.id) {
          this.getUser();
        }
      })
    );
    this.allSubscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe(() => {
          if (this.router.url === `/users/${this.id}/edit`) {
            this.usersService.opened = true;
          } else {
            this.usersService.opened = false;
          }
        })
    );
  }

  ngOnInit(): void {
    this.usersForm = new FormGroup({
      id: new FormControl(this.id),
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      role: new FormControl('', [Validators.required, Validators.minLength(2)]),
    });
    this.getUser();
    this.editUser(this.usersForm.value);
  }

  getUser() {
    this.store.select(getCurrentUser).subscribe(
      (data) => {
        this.user = data;
        this.editUser(data);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  editUser(data: User) {
    this.usersForm.patchValue({
      id: data.id,
      name: data.name,
      email: data.email,
      role: data.role,
    });
  }

  save(originalUser: User) {
    if (this.usersForm.valid) {
      const userObj: User = {
        ...originalUser,
        ...this.usersForm.value,
      };
      this.store.dispatch(UserAction.updateUser({ user: userObj }));
    }

    this.store.dispatch(UserAction.setCurrentUser({ currentUserId: this.id }));
    this.router.navigate(['/users']);
    this.drawerService.close();
  }

  onCancel() {
    const dialogRef = this.dialog.open(ModalConfirmComponent);
    this.allSubscriptions.push(
      dialogRef.afterClosed().subscribe(() => {
        this.usersService.opened = false;
        this.router.navigate(['/users']);
        this.drawerService.close();
      })
    );
    this.allSubscriptions.push(dialogRef.afterClosedStay().subscribe(() => {}));
  }

  closeDrawer() {
    this.drawerService.close();
  }

  ngOnDestroy(): void {
    this.allSubscriptions.forEach((sub) => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }
}
