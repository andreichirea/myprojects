import { Story, Meta } from '@storybook/angular/types-6-0';
import { InputEmailComponent } from 'src/app/shared/input-email/input-email.component';
export default {
  title: 'Example/formInput',
  component: InputEmailComponent,
} as Meta;

const Template: Story<InputEmailComponent> = (args: InputEmailComponent) => ({
  props: args,
});

export const Primary = Template.bind({});
Primary.args = { placeholderName: 'placeholder text' };
