import { Story, Meta } from '@storybook/angular/types-6-0';
import { NavigationComponent } from 'src/app/components/navigation/navigation.component';
export default {
  title: 'Example/navigation',
  component: NavigationComponent,
} as Meta;

const Template: Story<NavigationComponent> = (args: NavigationComponent) => ({
  props: args,
});

export const Primary = Template.bind({});
Primary.args = {};
