import { Story, Meta } from '@storybook/angular/types-6-0';
import { TransPriceComponent } from 'src/app/components/transactions/trans-price/trans-price.component';
import { MatMenuModule } from '@angular/material/menu';
export default {
  title: 'Example/TransPriceComponent',
  component: TransPriceComponent,
} as Meta;

const Template: Story<TransPriceComponent> = (args: TransPriceComponent) => ({
  props: args,
});

export const Primary = Template.bind({});
Primary.args = {
  name: 'Andrei',
  data: '23',
  title: 'Task Completed',
};

export const Secondary = Template.bind({});
Secondary.args = {
  name: 'Mircea',
  data: '21',
  title: 'Task Failed',
};

export const Large = Template.bind({});
Large.args = {
  name: 'Alin',
  data: '31',
  title: 'Years old',
};

export const Small = Template.bind({});
Small.args = {
  name: 'Mihnea',
  data: 'mihnea@gmail.com',
  title: 'Email',
};
