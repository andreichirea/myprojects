# paymentsPortal

in this application I tried to include as many technical notions as possible from the angular framework

features on this app :
-auth with json server
-crud with json server with users and transactions
-multy language
-services / forms / responsive layouts for 4 screen devices
-state management with ngrx - redux; subjects and behaviour subjects
-storybooks
-migrate @angular 14->15

node v:18.10.0;
npm install -g json-server;
npm install -D json-server json-server-auth

#1 start app with : ng serve -o ;
#2 start server for db and auth : json-server db.json -m ./node_modules/json-server-auth
