$(document).ready(function() {
    $('.green-banner').slick({

        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true

    });
    $('.slick-slides').slick({

        infinite: true,
        speed: 500,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000,
        dots: true

    });
    $('.bullets').ready(function() {
        $('.slick-next').html('<i class="fa-solid fa-angle-right"></i>').css({
            'color': '#98E5D6',
            'border': 'none',
            'font-size': '40px'
        })
        $('.slick-prev').html('<i class="fa-solid fa-angle-left"></i>').css({
            'color': '#98E5D6',
            'border': 'none',
            'font-size': '40px'
        })
    })


    //slider-splash-people

    // var width = $(window).width();
    // $(window).resize(function() {
    //     if (width <= 720) {
    //         $('.roundels').addClass('single-item');
    //     } else {
    //         $('.roundels').removeClass('single-item');
    //     }
    // });



    // toggleSlick = function() {
    //     if ($(window).width() < 770) {
    //         $('.single-item').slick({
    //             infinite: true,
    //             arrows: true,
    //             slidesToShow: 2,
    //         });
    //     } else {
    //         $('.single-item').unslick;
    //     }
    // }

    // $(window).resize(toggleSlick);
    // toggleSlick();
    $('.single-item').slick({
        responsive: [{
                breakpoint: 5000,
                settings: 'unslick'
            },
            {
                breakpoint: 770,
                settings: {
                    infinite: true,
                    arrows: true,
                    slidesToShow: 2,
                }


            },
            {
                breakpoint: 556,
                settings: {
                    infinite: true,
                    arrows: true,
                    slidesToShow: 1,
                }


            }

        ]

    })


    $(".accordition").click(function() {
        console.log('works')
        console.log(this)
        $(this).children('.accordition-hide').toggle();
        $(this).children('.title-bnr').children('.change').text(function(i, text) {
            return text === "+" ? "-" : "+";
        })
    });


    var modal = document.getElementById("myModal");
    var btn = document.getElementsByClassName("btn-article")[0];
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function() {
        modal.style.display = "block";
        console.log('ffff')
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    //modal2
    var modal2 = document.getElementsByClassName('myModal2')[0];
    var btn2 = document.getElementsByClassName("btn-article2")[0];
    var span2 = document.getElementsByClassName("close2")[0];
    console.log(modal2)
    btn2.onclick = function(e) {
        e.preventDefault()
        modal2.style.display = "block";
        console.log('ffff')
    }

    span2.onclick = function(e) {
        e.preventDefault()
        modal2.style.display = "none";
        console.log('ffdd')
    }

    window.onclick = function(event) {
        if (event.target == modal2) {
            modal2.style.display = "none";
        }
    }

    //navbar

    $(window).scroll(function() {
        if ($(this).scrollTop() > 10) {
            console.log('scroll')
            $('.header').css('height', '60px');
            $('.banner').css('margin-top', '60px');
            $('header img').css('width', '90px')
            $('.nav-hide').css('top', '40px')
        } else {
            $('.header').css('height', '80px');
            $('.banner').css('margin-top', '80px');
            $('header img').css('width', '138px')
            $('.nav-hide').css('top', '60px')
        }
    })

    $('.icn').click(function() {
        $('.icn').html($(this).html() == '<i class="fa-solid fa-bars"></i>' ? '<i class="fa-solid fa-x"></i>' : '<i class="fa-solid fa-bars"></i>');
        $('.nav-hide').slideToggle();
        $('.header img').css('width', '120px')
    })

    //dark-mode

    let theme = localStorage.getItem('data-theme') || 'light';
    const checkbox = document.getElementById("switch");
    const changeThemeToDark = () => {
        localStorage.setItem("data-theme", "dark")
        document.querySelector('body').classList.add('dark-mode')
    }
    const changeThemeToLight = () => {
        localStorage.setItem("data-theme", "light");
        document.querySelector('body').classList.remove('dark-mode')
    }

    if (theme === 'dark') {
        changeThemeToDark();
        checkbox.checked = true

    }
    checkbox.addEventListener('change', (e) => {
        let theme = localStorage.getItem('data-theme');
        if (e.target.checked === false) {
            changeThemeToLight()
        } else {
            changeThemeToDark()
        }
    });

    //apiCall

    document.getElementById('submit').onclick = function() {
        var API_KEY = '27024083-ebdc94bbdda3e3bb711d789f9';
        var myFruit = document.getElementById('fruit').value;
        var name = document.getElementById('name').value;
        let headerText = document.getElementById('fname');
        var URL = "https://pixabay.com/api/?key=" + API_KEY + "&q=" + myFruit;

        console.log(myFruit)
        $.getJSON(URL, function(data) {
            if (parseInt(data.totalHits) > 0)
                $.each(data.hits, function(i, hit) {
                    document.getElementById('imgAppend').src = hit.largeImageURL;
                    $('.form-hide').css('display', 'block');
                    $('.formm').css('margin-bottom', '150px');
                    $(headerText).text(`Thank you, ${name}`)
                    console.log(hit.largeImageURL);
                });
            else
                console.log('No hits');
        });

    }

});