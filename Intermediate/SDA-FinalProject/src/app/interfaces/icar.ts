export interface Icar {
    "id": 5,
    "firma": string,
    "model":string,
    "pret": string,
    "an": string,
    "km": string,
    "tipCaroserie": string,
    "culoare": string,
    "capacitateCilindrica": string,
    "caiPutere": string,
    "combustibil": string,
    "taraOrigine": string,
    "cutieViteze": string,
    "primulProprietar": string,
    "faraAccident": string,
    "carteService":string,
    "normaPoluare": string,
    "img1":string,
    "img2":string,
    "img3":string,
    "img4":string,
    "numarPortiere": string,
    "optiuni":string
}
