import { Component, OnInit } from '@angular/core';
import { IArticle } from 'src/app/interfaces/iarticle';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ArticlesService } from 'src/app/services/articles.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  articles: IArticle[] = [];
  articleForm!: FormGroup;
  validMessage:string = "";

  constructor(private articlesServices: ArticlesService) { }

  readArticles(){
    return this.articlesServices.getArticles().subscribe(
      (data) => {this.articles = data},
      (err) => {console.log(err)}
    )
  }

  createForm(){
    this.articleForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      review: new FormControl('', [Validators.required]),
      
    });
  }
  
  submitArticle(){
    if(this.articleForm.valid){
    this.validMessage="Recenzia dumneavoastra a fost adaugata cu succes.";
    this.articlesServices.createArticle(this.articleForm.value).subscribe(
      (data) => {
        this.articleForm.reset(data);
        console.log(data);
        this.readArticles();
      },
      (err) => {console.log(err)}
    )
    }
    else {
      this.validMessage = "Va rugam introduceti informatii in toate text-inputs"
    }
  }

  ngOnInit(): void {
    this.readArticles();
    this.createForm();
  }
  
}
