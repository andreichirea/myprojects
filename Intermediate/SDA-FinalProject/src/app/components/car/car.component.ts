import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Icar } from 'src/app/interfaces/icar';
import { CarsService } from 'src/app/services/cars.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  carData!:Icar ;
  id!:number;
  
  constructor(private serviceCars: CarsService,private route:ActivatedRoute ) { }

  ngOnInit(): void {
    this.showCar(this.id);
  }

  showCar(id:number){
    this.id =this.route.snapshot.params['carId'];
    return this.serviceCars.getACarBtId(this.id).subscribe(
      (data)=>{this.carData=data;
      console.log(this.carData)},
      (err)=>{console.log(err)}
    )
  }

}
