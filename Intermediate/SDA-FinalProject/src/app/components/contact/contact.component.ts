import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogModule,MAT_DIALOG_DATA} from '@angular/material/dialog';
//import * from jquery
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  
  constructor(public dialog: MatDialog) { }
 
  ngOnInit(): void {  $(".click").click(function(e) {
    e.preventDefault();
    var aid = $(this).attr("href");
    $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
  });
  }

}
