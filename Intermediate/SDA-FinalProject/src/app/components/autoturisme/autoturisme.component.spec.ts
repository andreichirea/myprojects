import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoturismeComponent } from './autoturisme.component';

describe('AutoturismeComponent', () => {
  let component: AutoturismeComponent;
  let fixture: ComponentFixture<AutoturismeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoturismeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoturismeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
