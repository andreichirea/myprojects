import { ChangeDetectorRef, Component, OnInit, ViewChild ,OnDestroy,AfterViewInit } from '@angular/core';
import { Icar } from 'src/app/interfaces/icar';
import { CarsService } from 'src/app/services/cars.service';
import { MatPaginator} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-autoturisme',
  templateUrl: './autoturisme.component.html',
  styleUrls: ['./autoturisme.component.scss']
})
export class AutoturismeComponent implements OnInit , AfterViewInit, OnDestroy{
  @ViewChild("paginator") paginator: MatPaginator;
  brands=['Bmw','Audi','Mercedes','Porsche'];
  models =['seria 7','M2','Q8','A8','A5','Q3','e-tron','seria 4','R8','S-class','A-class','CLA','AMG GT-R','AMG GT','Cayenne'];
  allBrands = {
    'Bmw': ['seria 7','M2', 'seria 4'],
    'Audi': ['Q8','A8','A5','Q3','e-tron','R8'],
    'Mercedes':['S-class','A-class','CLA','AMG GT-R','AMG GT'],
    'Porsche':['Cayenne']
  };
  modelsByBrand= this.models;
  selectedBrand:any;
  selectedModel:any;
  cars:Icar[]=[];
  
  carByModel:Icar[]=[];


  carsObs: Observable<any>;
  carFilter:Icar[]=[];
  dataSource: MatTableDataSource<Icar> = new MatTableDataSource<Icar>(this.carFilter);


  constructor(private serviceCars:CarsService,
              private changeDetectorRef: ChangeDetectorRef) { }
  ngOnDestroy(): void {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.getCars();
  }

  onBrandChange(){
    console.log(this.selectedBrand);
    this.models = this.allBrands[this.selectedBrand]
    this.carFilter = this.cars.filter(car => car.firma === this.selectedBrand);
    this.dataSource.data = this.carFilter;
  }
  onModelChange(){
    console.log(this.selectedModel)
    this.carByModel = this.carFilter.filter(car =>car.model === this.selectedModel);
    this.dataSource.data = this.carByModel;
  }

  getCars(){

    this.serviceCars.getAllCars().subscribe(
      (data: any) => {
        this.cars = data;
        //  Make a copy of the cars array
        this.carFilter = JSON.parse(JSON.stringify(this.cars));
        this.dataSource = new MatTableDataSource<Icar>(data);
        this.dataSource.data = data;
        console.log(this.carFilter)
        this.dataSource.paginator = this.paginator;
        console.log(this.carsObs)
        this.changeDetectorRef.detectChanges();
        this.carsObs = this.dataSource.connect();    
      },
      (err) => { console.log(err) }
    )
  }
  

}

