import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Icar } from '../interfaces/icar';
@Injectable({
  providedIn: 'root'
})
export class CarsService {

  apiUrl = 'http://localhost:3000';
  httpHeaders = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  getAllCars():Observable<Icar[]>{
    return this.http.get<Icar[]>(this.apiUrl + '/cars');
  }
  getACarBtId(id:number):Observable<Icar>{
    return this.http.get<Icar>(this.apiUrl + '/cars/'+id);
  }

}