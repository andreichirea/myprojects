import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IArticle } from '../interfaces/iarticle';

import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  private apiURL = "http://localhost:3000/reviews";

  httpHeaders = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  getArticles():Observable<IArticle[]>{
    return this.http.get<IArticle[]>(this.apiURL);
  }
  createArticle(article: IArticle) :Observable<IArticle>
    {
      return this.http.post<IArticle>
      (
        this.apiURL , 
        JSON.stringify(article), 
        this.httpHeaders
      )
      .pipe
      (
        retry(1),
        
      )
    }

}
