import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoturismeComponent } from './components/autoturisme/autoturisme.component';
import { BuyBackComponent } from './components/buy-back/buy-back.component';

import { CarComponent } from './components/car/car.component';
import { ContactComponent } from './components/contact/contact.component';
import { CrewComponent } from './components/crew/crew.component';
import { DeliveredComponent } from './components/delivered/delivered.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { OrdersComponent } from './components/orders/orders.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { ReviewComponent } from './components/review/review.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'cars',
    component: AutoturismeComponent,
    pathMatch: 'full'
  },
  {
    path: 'car/:carId',
    component: CarComponent,
    pathMatch: 'full'
  },
  {
    path: 'review',
    component: ReviewComponent,
    pathMatch: 'full'
  },
  {
    path: 'crew',
    component: CrewComponent,
    pathMatch: 'full'
  },
  {
    path: 'buyBack',
    component: BuyBackComponent,
    pathMatch: 'full'
  },
  {
    path: 'contact',
    component: ContactComponent,
    pathMatch: 'full'
  },
  {
    path: 'delivered',
    component: DeliveredComponent,
    pathMatch: 'full'
  },
  {
    path: 'orders',
    component: OrdersComponent,
    pathMatch: 'full'
  },
  {
    path: 'questions',
    component: QuestionsComponent,
    pathMatch: 'full'
  },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
