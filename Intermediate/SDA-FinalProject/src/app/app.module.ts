import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatDialogModule} from '@angular/material/dialog';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AutoturismeComponent } from './components/autoturisme/autoturisme.component';
import { MatIconModule } from '@angular/material/icon';
import { CarComponent } from './components/car/car.component';
import { OrdersComponent } from './components/orders/orders.component';

import { CrewComponent } from './components/crew/crew.component';
import { DeliveredComponent } from './components/delivered/delivered.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { ContactComponent } from './components/contact/contact.component';
import {MatSelectModule} from '@angular/material/select';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ReviewComponent } from './components/review/review.component';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AutoturismeComponent,
    CarComponent,
    OrdersComponent, 
    CrewComponent,
    DeliveredComponent,
    QuestionsComponent,
    ContactComponent,
    ReviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule
  
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


