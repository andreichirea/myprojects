//SELECTOR
const todoInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoList = document.querySelector('.todo-list');
const filterOption = document.querySelector('.filter-todo');

//event listeners
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteCheck);
filterOption.addEventListener('change', filterTodo);


//function

function addTodo(event) {
    event.preventDefault();
    const todoDiv = document.createElement('div');
    todoDiv.classList.add('todo');
    const newTodo = document.createElement('li');
    newTodo.innerText = todoInput.value;
    newTodo.classList.add('todo-item');
    todoDiv.appendChild(newTodo);

    //check btn
    const completedBut = document.createElement('button');
    completedBut.innerHTML = '<i  class="fas fa-check" > </i>';
    completedBut.classList.add('complete-btn');
    todoDiv.appendChild(completedBut);

    //trash btn
    const trashBut = document.createElement('button');
    trashBut.innerHTML = '<i  class="fas fa-trash" > </i>';
    trashBut.classList.add('trash-btn');
    todoDiv.appendChild(trashBut);

    todoList.appendChild(todoDiv);

    //cleartodoInput value 
    todoInput.value = '';
}


function deleteCheck(e) {
    const item = e.target;

    if (item.classList[0] === 'trash-btn') {
        const todo = item.parentElement;
        //animation
        todo.classList.add('fall');
        todo.addEventListener('transitionend', function() {
            todo.remove();
        })

    }
    //check mark

    if (item.classList[0] === 'complete-btn') {
        const todo = item.parentElement;
        todo.classList.toggle('completed');
    }
}

function filterTodo(e) {
    const todos = todoList.childNodes; //queryselectall
    console.log(todos)
    for (todo of todos) {
        console.log(todo)
        switch (e.target.value) {
            case "all":
                todo.style.display = "flex";
                break;
            case "completed":
                if (todo.classList.contains('completed')) {
                    todo.style.display = "flex";

                } else {
                    todo.style.display = "none";
                }
            case "uncompleted":
                if (!todo.classList.contains('completed')) {
                    todo.style.display = "flex";

                } else {
                    todo.style.display = "none";
                }
        }

    }

}
console.log(todoList.childNodes);