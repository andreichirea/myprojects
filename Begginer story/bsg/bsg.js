class Laptop {
    constructor(pret, memorie, ram, procesor, an, descriere, firma, model, image) {
        this.pret = pret;
        this.memorie = memorie;
        this.ram = ram;
        this.procesor = procesor;
        this.an = an;
        this.descriere = descriere;
        this.firma = firma;
        this.model = model;
        this.image = image;


    }

}

laptops = [];

laptops.push(new Laptop(2250, 500, 4, 'i7 87700 ghz', 2020, 'Acest laptop este ideal pentru un uz zilnic de birou , are SSD ceea ce face ca fisierele sa se incarce foarte rapid , este micut si usor de purtat oriunde cu DV.', 'Msi', 'gf 63 8 rc', 'msi.webp'));
laptops.push(new Laptop(3000, 500, 8, 'i5 6500 ghz', 2021, 'Acest laptop este de gaming ,avand o placa grafica foarte putenica de la Nvidia, ruleaza orice joc la 100+fps', 'Asus', 'Rog Strix', 'rog.jpg'));
laptops.push(new Laptop(1500, 500, 4, 'i5 4500ghz', 2019, 'Este un laptop bun, cu care iti poti face treaba foarte bine de oriunde , bateria tine intre 7-9h depinde de utilizare', 'Sony', 'Vayo', 'vayo.jpg'));
laptops.push(new Laptop(4000, 500, 12, 'i7', 2021, 'Apple Mackbook pro este destinat persoanelor de bussines ,de ofice atat cat si dj-ilor care il folosesc la capacitate maxima!', 'Apple', 'Mackbook pro II', 'mackbook.webp'))

console.log(laptops);
window.onload = function() {
    document.getElementsByClassName('contact-auto')[0].onclick = function(event) {
        event.preventDefault();
        document.getElementsByClassName('contact-phones')[0].innerHTML = ``;
        document.getElementsByClassName('contact-phones')[0].innerHTML = `Numarul de contact pentru parcul nostru auto este 0735928640`;

    }



    document.getElementsByClassName('contact-lpt')[0].onclick = function(event) {
        event.preventDefault();
        document.getElementsByClassName('contact-phones')[0].innerHTML = ``;
        document.getElementsByClassName('contact-phones')[0].innerHTML = `Numarul de contact pentru categoria laptopuri este 0738964384`

    }






    document.getElementsByClassName('contact-tells')[0].onclick = function(event) {
        event.preventDefault();
        document.getElementsByClassName('contact-phones')[0].innerHTML = '';
        document.getElementsByClassName('contact-phones')[0].innerHTML = `
     Numarul de contanct pentru telefoanele noastre este 0745983341`

    }
    document.getElementsByClassName('laptops')[0].onclick = function(event) {
        event.preventDefault();
        document.getElementsByClassName('carduri-olx')[0].innerHTML = '';
        for (let i = 0; i < laptops.length; i++) {
            document.getElementsByClassName('carduri-olx')[0].innerHTML += `

    
            <div class="col-lg-3 col-md-5 col-sm-6 offset-1 cards">
        <div class="card h-100" style="width: 18rem;">
  <img src="img/${laptops[i].image}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">${laptops[i].firma} ${laptops[i].model}</h5>
    <p class="card-text">${laptops[i].descriere}</p>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item"> Memoria Ram este de ${laptops[i].ram} gb.</li>
    <li class="list-group-item">Procesorul este  ${laptops[i].procesor}</li>
    <li class="list-group-item">Memoria interna este de ${laptops[i].memorie} gb.</li>
    <li class="list-group-item">Aceste laptop a fost fabricat in ${laptops[i].an}</li>
  </ul>
  <div class="card-body">
  
  <a href="#" class="card-link">Contacteaza vanzatorul!</a>
  <br>
  <a href="#" class="card-link">Mai multe detalii!</a>
  <br><br>
    <a href="#" class="card-link">${laptops[i].pret} Ron</a>
    <a href="#" class="card-link btn btn-success">Cumpara!</a>
    
  </div>
</div>
</div>
     
     <br>`
        }
    }
    document.getElementsByClassName('tells')[0].onclick = function(event) {
        event.preventDefault();
        document.getElementsByClassName('carduri-olx')[0].innerHTML = ' ';
        for (let i = 0; i < phones.length; i++) {
            document.getElementsByClassName('carduri-olx')[0].innerHTML += `
          <div class="col-lg-3 col-md-5 col-sm-6 offset-1 cards">
        <div class="card h-100" style="width: 18rem;">
  <img src="img/${phones[i].img}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">${phones[i].firma} ${phones[i].model}</h5>
    <p class="card-text">${phones[i].descriere}</p>
  </div>
  <ul class="list-group list-group-flush">
    
    <li class="list-group-item">Bateria este de ${phones[i].baterie} mA</li>
    <li class="list-group-item">Memoria interna este de ${phones[i].memorie} gb.</li>
    <li class="list-group-item">Aceste telefon a fost fabricat in ${phones[i].an}</li>
  </ul>
  <div class="card-body">
  
    <a href="#" class="card-link">Contacteaza vanzatorul!</a>
    <br>
    <a href="#" class="card-link">Mai multe detalii!</a>
    <br><br>
    <a href="#" class="card-link">${phones[i].pret} Ron </a>
    <a href="#" class="card-link btn btn-success">Cumpara!</a>
    
  </div>
</div>
</div>
     
     <br>`






        }

    }
    document.getElementsByClassName('masini')[0].onclick = function(event) {
        event.preventDefault();
        document.getElementsByClassName('carduri-olx')[0].innerHTML = ' ';
        for (let i = 0; i < cars.length; i++) {
            document.getElementsByClassName('carduri-olx')[0].innerHTML += `
        <div class="col-lg-3 col-md-5 col-sm-6 offset-1 cards">
        <div class="card h-100" style="width: 18rem;">
  <img src="img/${cars[i].img}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">${cars[i].firma} ${cars[i].model}</h5>
    <p class="card-text">${cars[i].descriere}</p>
  </div>
  <ul class="list-group list-group-flush">
    
    <li class="list-group-item">Kilometraj:${cars[i].km}</li>
    <li class="list-group-item">Culoarea masinii este ${cars[i].culoare}</li>
    <li class="list-group-item">Aceasta masina a fost fabricat in ${cars[i].an}</li>
  </ul>
  <div class="card-body">
  
    <a href="#" class="card-link">Contacteaza vanzatorul!</a>
    <br>
    <a href="#" class="card-link">Mai multe detalii!</a>
    <br><br>
    <a href="#" class="card-link">${cars[i].pret} Euro </a>
    <a href="#" class="card-link btn btn-success">Cumpara!</a>
    
  </div>
</div>
</div>
     
     <br>
        
        
        `
        }
    }
}

//class telefoane 
class Telefon {
    constructor(pret, firma, model, an, memorie, baterie, descriere, img) {
        this.pret = pret;
        this.firma = firma;
        this.model = model;
        this.an = an;
        this.memorie = memorie;
        this.baterie = baterie;
        this.descriere = descriere;
        this.img = img;

    }



}
let phones = [];
phones.push(new Telefon(1600, 'Asus', 'Rog', 2019, 128, 5000, 'Un telefon destinat jocului pe mobil, are specificatii foarte bune si merge la full-resolution orice joc pentru mobil.', 'rog-tel.jpeg'))
phones.push(new Telefon(3200, 'Samsung', ' Galaxy s21 ULTRA', 2021, 512, 4500, 'Acest model vine cu imbunatatiri drastice fata de modelul precedent, puntul forte al telefonului este camera care poate avea x100 zoon in', 's21.jpg'));
phones.push(new Telefon(4500, 'Apple', 'iphone 12', 2020, 128, 3800, 'Este flagshipul celor de la Apple, pe langa specificatiile foarte bune, acest model a reluat traditia celor de la Apple, de la modelul Iphone 5 cu marginile drepte, fapt ce ii bucura pe multi utilizatori', '12.png'));
phones.push(new Telefon(2300, 'Huawey', 'p30 Pro', 2019, 128, 5000, 'Acest model a fost o buna vreme cel mai bun Model al celor de la Huawey, el avand o camera extraordinara si un display foarte bun', 'p30.webp'));
phones.push(new Telefon(1400, 'Samsung', 'Galaxy s10', 2018, 128, 3900, 'Este telefonul meu personal de 2 ani si sunt foarte multumit de el, bateria tine o zi intreaga iar functiile specifice merg perfect', 's10.jpg'));
phones.push(new Telefon(1300, 'Samsung', 'A51', 2020, 64, 3500, 'Acest model este unul foarte accesibil pentru toata lumea si il recomand deoarece ruleaza foarte bine pe orice tip de task!', 'a51.jpg'))

//clasa masini 
class Car {
    constructor(pret, firma, model, km, culoare, descriere, img, an) {
        this.pret = pret;
        this.firma = firma;
        this.model = model;
        this.km = km;
        this.culoare = culoare;
        this.descriere = descriere;
        this.img = img;
        this.an = an;
    }
}

let cars = [];
cars.push(new Car(10500, 'Audi', 'a5', 96550, 'alba', 'Acest model este destinat tinerilor in special , masina este intretitnuta excesiv de catre un pasionat, revizii la 10k km , s a schimbat uleiu la cutia de viteze', 'a5.jpg', 2014));
cars.push(new Car(15000, 'Bmw', 'f30 328i', 122500, 'albastru', 'O masina foarte frumoasa si destul de puternica pentru orice gust, reviziile au fost facute mereu la Bavaria , am schimbat distributia. Masina ruleaza excelent, fara defecte ascunse.', '328i.jpg', 2013));
cars.push(new Car(90000, 'Bmw', "M8c", 20000, 'rosie', 'Cea mai puternica masina creata vreodata de BMW, are 20mii de km, deci este intr-o stare excelenta, fara modificari mecanice', 'm8.jpg', 2021));
cars.push(new Car(170000, 'Lamborghini', 'Huracan', 20000, 'verde', 'Super-car la un pret foarte bun, masina este aproape noua, reviziile facute regulat la reprezentanta, accept si Crypto', 'huracan.jpg', 2019));
cars.push(new Car(3500, 'Mazda', 'Rx-8', 94.000, 'neagra', 'O masina de "colectie" destinata pasionatilor de motoare, ea are un motor boxer de 1.3L ce produce 230hp, este interetinuta exemplar si nu s-a mers foarte mult cu ea deoarece a fost a3a masina de familie', 'rx-8.jpg', 2005));
cars.push(new Car(22000, 'Mercedes', 'C-class', 75400, 'neagra', 'Masia provinte de la reprezentanta Mercedes, are foarte multe optiuni deoarece era masina de showroom, servisata la timp, imprecabila din orice punct de vedere',
    'c-classs.jpg', 2017))